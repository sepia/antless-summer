ARG app_abbrev
ARG app_version
ARG build_dir=/build/
ARG download_dir=/dwnld/


FROM debian:buster-slim AS builder

# use bash for better debugging capabilities, e.g. pipefail support
SHELL [ "/bin/bash", "-c" ]

# hadolint ignore=DL3008
RUN apt-get update \
  && apt-get install \
    --yes \
    --no-install-recommends \
    ca-certificates \
    wget \
    # envsubst
    gettext-base \
    zip

ARG download_dir
WORKDIR ${download_dir}
COPY dependencies.txt .
# Download dependency packages
RUN xargs \
    --max-args=1 \
    --max-procs=4 \
    wget \
      --no-verbose \
      --show-progress \
      --tries=3 \
    < dependencies.txt \
    && rm dependencies.txt

ARG build_dir
WORKDIR ${build_dir}
COPY src/ .
COPY substitutions.txt .

ARG app_version
ARG app_abbrev
ARG app_web
# build ARGs variable substitutions to certain files
# hadolint ignore=SC2002,DL4006
RUN set -o pipefail; \
  cat substitutions.txt \
  # build sed script commands from variable names
  # hack: use a custom delimiter (^) for sed to allow "//" in url substitutions
  | xargs -I[] echo "s^@[]@^\${[]}^g" \
  # substitute build ARGs with their values
  | envsubst \
  # run the sed script commands on the files
  | xargs -I[] sed -i [] expath-pkg.xml repo.xml \
  # finally, prevent the inclusion of the substutions file into the archive
  && rm substitutions.txt

# zip it
RUN zip -r ${app_abbrev}-${app_version}.xar ./*


FROM docker.gitlab.gwdg.de/sepia/existdb-images/existdb:5.2.0-6e8c51a0-debug

ARG download_dir
COPY --from=builder ${download_dir} /exist/autodeploy

ARG app_abbrev
ARG app_version
ARG build_dir
COPY --from=builder ${build_dir}${app_abbrev}-${app_version}.xar /exist/autodeploy
