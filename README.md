# Antless Summer

A template and example-app for ant-less expath package builds.

## Dependencies

Declare package dependencies in a file called `dependencies.txt` by providing download links to the packages each on one line of the file.

exist-db does not respect the `versions`, `semver`, `semver-min` and `semver-max` attributes for the dependencies declared in `expath-pkg.xml` during (auto-)deployment but this is totally in accordance with the EXPath [Packaging System specification](http://expath.org/spec/pkg). You SHOULD NOT rely upon the declarations you make in that file but instead care for yourself that the correct versions are provided with in the `dependencies.txt`.

As of exist-db 5.3.0 the dependencies delivered with the base image are:

- dashboard-2.0.8.xar
- eXide-3.0.0.xar
- exist-documentation-5.3.0.xar
- exist-function-documentation-1.2.0.xar
- functx-1.0.1.xar
- ~~markdown-0.6.xar~~
- monex-3.0.3.xar
- packageservice-1.3.12.xar
- semver-xq-2.3.0.xar
- ~~shared-resources-0.8.5.xar~~
- templating-1.0.2.xar

If you would like to provide another version of one of these dependencies in your image, you have to remove the xar-file from the base image or build your own from scratch.

## Build arguments substitutions

Declare variable names that need to be substituted into files (currently `expath-pkg.xml`, `repo.xml`) and whose values are only known at build time (like a version number) in the `substitutions.txt`, each on one line. For the variables to substitute correcly, add them as `ARG` to the `Dockerfile`  and provide proper values build time.

----

:warning: Do not use the caret character (`^`) in any of the values because this is used as a delimiter during processing.

----

Build arguments that are provided with this setup are:

- app_abbrev
- app_version
- app_web

## Development

1. Install dependencies.

    ```sh
    npm ci
    ```

1. Code.

1. Commit (`git commit`).

## Remarks considering code quality

This repo is [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) and uses [husky🐶](https://www.npmjs.com/package/husky) to configure pre-commit `Dockerfile` linting with [hadolint](https://github.com/hadolint/hadolint) (needs docker-cli to be installed).
