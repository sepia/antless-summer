xquery version "3.0";

(:~
 : A set of helper functions to access the application context from
 : within a module.
 :)
module namespace config="http://sepia.io/antless-summer/config";

declare namespace expath="http://expath.org/ns/pkg";
declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace templates="http://exist-db.org/xquery/templates";

(:
    Determine the application root collection from the current module load path.
:)
declare variable $config:app-root :=
    let $rawPath := system:get-module-load-path()
    let $modulePath :=
        (: strip the xmldb: part :)
        if (starts-with($rawPath, "xmldb:exist://")) then
            if (starts-with($rawPath, "xmldb:exist://embedded-eXist-server")) then
                substring($rawPath, 36)
            else
                substring($rawPath, 15)
        else
            $rawPath
    return
        substring-before($modulePath, "modules")
;

(:~
 : Check for the existence of the repo.xml file before asigning its contents.
 : Raises an error if the file does not exist, which is usually the case when
 : the app was not deployed correctly.
 :)
declare variable $config:repo-descriptor :=
  if ( fn:doc-available(fn:concat($config:app-root, "repo.xml")) )
  then
    fn:doc(concat($config:app-root, "repo.xml"))/repo:meta
  else
    fn:error(xs:QName("err:FODC0002"), fn:concat("Error retrieving resource ", $config:app-root, "repo.xml"))
  ;

declare variable $config:expath-descriptor := doc(concat($config:app-root, "/expath-pkg.xml"))/expath:package;

(:~
 : Returns the repo.xml descriptor for the current application.
 :)
declare function config:repo-descriptor() as element(repo:meta) {
    $config:repo-descriptor
};

(:~
 : Returns the expath-pkg.xml descriptor for the current application.
 :)
declare function config:expath-descriptor() as element(expath:package) {
    $config:expath-descriptor
};

declare %templates:wrap function config:app-title($node as node(), $model as map(*)) as text() {
    $config:expath-descriptor/expath:title/text()
};

declare function config:app-meta($node as node(), $model as map(*)) as element()* {
    <meta xmlns="http://www.w3.org/1999/xhtml" name="description" content="{$config:repo-descriptor/repo:description/text()}"/>,
    for $author in $config:repo-descriptor/repo:author
    return
        <meta xmlns="http://www.w3.org/1999/xhtml" name="creator" content="{$author/text()}"/>
};

(:~
 : Generates a table showing all properties defined
 : in the application descriptors.
 :)
declare %templates:wrap function config:app-info($node as node(), $model as map(*)) {
    <table class="app-info">
        <tr>
            <td>app collection:</td>
            <td>{$config:app-root}</td>
        </tr>
        {
            for $attr in ($config:expath-descriptor/@*, $config:expath-descriptor/*, $config:repo-descriptor/*)
            return
                <tr>
                    <td>{node-name($attr)}:</td>
                    <td>{if ($attr/string()) then $attr/string() else for $each in $attr/@* return concat(node-name($each), "=", $each/string()) }</td>
                </tr>
        }
        <tr>
            <td>Controller:</td>
            <td>{ request:get-attribute("$exist:controller") }</td>
        </tr>
    </table>
};